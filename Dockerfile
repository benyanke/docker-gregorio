FROM ubuntu:20.04
MAINTAINER Ben Yanke "ben@benyanke.com"

ENV GREGORIO_VERSION 6.0.0
ENV DEBIAN_FRONTEND noninteractive

# Install Texlive and various tex bits
RUN apt-get update -y \
      && apt-get install -y \
            texlive \
            texlive-latex-extra \
            texlive-latex-recommended \
            texlive-fonts-extra \
            texlive-humanities \
            texlive-music \
            texlive-luatex \
            texlive-lang-all \
            fonts-sil-doulos \
            fonts-sil-charis \
            fonts-freefont-ttf \
            fonts-ebgaramond \
            fonts-cardo \
            fonts-opendyslexic \
            build-essential \
            wget \
            pandoc \
            python-yaml \
            python3-yaml \
            python3-pip \
      && rm -rf /var/lib/apt/lists/* && \
      pip install j2cli[yaml]

# Install other fonts
COPY ./assets/roman-liturgy-font/liturgy.ttf /usr/local/share/fonts/liturgy.ttf
COPY ./assets/gresym/gresym.ttf /usr/local/share/fonts/gresym.ttf
#COPY ./assets/webomints/WebOMintsGD.tfm /usr/local/share/fonts/WebOMintsGD.tfm
COPY ./assets/webomints/WebOMintsGD.ttf /usr/local/share/fonts/WebOMintsGD.ttf
RUN fc-cache -f -v

# Install Gregorio
WORKDIR /extract

RUN wget -q https://github.com/gregorio-project/gregorio/releases/download/v${GREGORIO_VERSION}/gregorio-${GREGORIO_VERSION}.tar.bz2 -O /tmp/gregorio.tar.gz2 && \
    tar --strip-components=1 -xf /tmp/gregorio.tar.gz2 && \
    ./build.sh && \
    ./install.sh

WORKDIR /build





