# Docker Gregorio

Docker image for Gregorio with texlive.

## Description
This image contains a full install of Gregorio and Tex Live for containerized compiling of LaTeX and Gregorio projects.

Also includes [j2cli](https://github.com/kolypto/j2cli) under the binary name `j2` to provide the ability to render tex documents
with jinja from within the container, making some CI workflows easier. For a usage example, see the tests that run in CI against
the newly-build container.

## Container Images

The following images are build by CI:

On tags for releases (git tags vX.X.X):
 - `registry.gitlab.com/benyanke/docker-gregorio:latest`
 - `registry.gitlab.com/benyanke/docker-gregorio:{tag}`

On tags for pre-releases (git tags vX.X.X-rcX):
 - `registry.gitlab.com/benyanke/docker-gregorio:release-candidate`
 - `registry.gitlab.com/benyanke/docker-gregorio:{tag}`

On every push to any branch:
 - `registry.gitlab.com/benyanke/docker-gregorio:branch_{branchname}`
 - `registry.gitlab.com/benyanke/docker-gregorio:commit_{commit_hash}`

Note that the `branch_*`, `commit_*`, and release candidate tags are cleaned up regularly.

## Release Process

Test the `commit_*` image after pushing the commit, and do local testing.

When ready to make a release, make a release candidate by making a tag
matching `vX.X.X-rcX` where `X` is an integer.


If the RC works, make a git tag matching the pattern `vX.X.X` where
`X` is an integer.

